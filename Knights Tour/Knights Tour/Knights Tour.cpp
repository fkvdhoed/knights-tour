#include <stdio.h>
#include <iostream>
#include <iomanip>

#include "Board.h"

int main(int argc, const char * argv[])
{
	Board b;
	char input[128];
	int r;
	int c;
	while (true)
	{
		while (true)  // while the user doesn't provide a correct position
		{
			printf("Enter position: row from a-h, colmn from 8-1, type r for random (exmpl f4)\n");
			std::cin.getline(input, 128);
			if (input[0] == 'r') //create random start position
			{
				srand(time(NULL));  //give rand() a diffrent seed
				r = rand() % 8;
				c = rand() % 8;
				break;
			}else if(((64 < (int)input[0] && (int)input[0] < 73) || (96 < (int)input[0] && (int)input[0] < 105))  //first char from a to h or from A to H
				&& (48 < (int)input[1] && (int)input[1] < 57))   //second char between 0 and 9
			{
				r = (int)input[0] - (96 < (int)input[0] ? 97 : 65);
				c = 7 - ((int)input[1] - 49);
				break;
			}else
			{
				printf("Incorrect input\n");
			}
		}
		printf("Solution\n");
		b.solve(BoardPosition(r, c));
		b.print();

		while (true) //while the user doesn't provide a YES or NO
		{
			printf("Again? Y\\N \n");
			std::cin.getline(input, 128);
			if (input[0] == 'Y' || input[0] == 'y')
			{
				b.reset();
				break;
			} else if (input[0] == 'N' || input[0] == 'n')
			{
				return 0;
			} else
			{
				printf("Incorrect input\n");
			}
		}
		
	}
}

