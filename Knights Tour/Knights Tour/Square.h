#ifndef KNIGHTSTOUR_BASE_HEADERFILES_SQUARE_H
#define KNIGHTSTOUR_BASE_HEADERFILES_SQUARE_H

#include "RouteType.h";

// Divines a square on the chess board
//
// Only used in combination with Board.h to solve the Kight's tour problem
class Square
{
	public:
		Square();
		Square(const int& quadrant, const RouteType& routeType);

		int getQuadrant();
		RouteType getRouteType();
		int getTurnVisited();
		bool getVisited();

		void setQuadrant(const int& value);
		void setRouteType(const RouteType& value);

		void reset();
		bool visit(const int& turn);

	private:
		int mQuadrant;
		RouteType mRouteType;
		int mTurnVisited;
		bool mVisited;
};

#endif // KNIGHTSTOUR_BASE_HEADERFILES_SQUARE_H