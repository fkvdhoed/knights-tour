#ifndef KNIGHTSTOUR_BASE_HEADERFILES_BOARDPOSITION_H
#define KNIGHTSTOUR_BASE_HEADERFILES_BOARDPOSITION_H

// Simple class to store the possion on the board.
// Class is uses to add and substract values for easy use.
// The possions are stored from 0-7 any other possision is renderd invalit but can still be stored in this class.
class BoardPosition
{
public:
	BoardPosition();
	BoardPosition(const int& column, const int& row);
	
	int getColumn() const;
	int getRow() const;
	void setColumn(int value);
	void setRow(int value);

	BoardPosition operator + (const BoardPosition& rhs) const;
	BoardPosition operator - (const BoardPosition& rhs) const;
	bool operator == (const BoardPosition& rhs) const;
	BoardPosition& operator += (const BoardPosition& rhs);
	BoardPosition& operator -= (const BoardPosition& rhs);

	// Assign both memberfunctions at once
	void set(const int& row, const int& column);

private:
	int mColumn;
	int mRow;
};

#endif // KNIGHTSTOUR_BASE_HEADERFILES_BOARDPOSITION_H