#ifndef KNIGHTSTOUR_BASE_HEADERFILES_BOARD_H
#define KNIGHTSTOUR_BASE_HEADERFILES_BOARD_H

#include "Square.h"
#include "BoardPosition.h"

// Algorithm to solves the Knights problem 
//
// This algorithm divides a 8x8 chess board in 4 subquadrant and 4 routes
// It first visit all the reachable squares in the quadrant
// then it will visit an other quadrant with the same RouteType before it will start a new route
//
//     a  b  c  d   e  f  g  h
//   ------------- ------------
// 8 |LD|LS|RS|RD| |LD|LS|RS|RD| 0 
//   |-----------| |-----------|   
// 7 |RS|RD|LD|LS| |RS|RD|LD|LS| 1 
//   |-----------| |-----------|   m
// 6 |LS|LD|RD|RS| |LS|LD|RD|RS| 2 S
//   |-----------| |-----------|   q
// 5 |RD|RS|LS|LD| |RD|RS|LS|LD| 3 a
//   ------------- -------------   r
//   ------------- -------------   e
// 4 |LD|LS|RS|RD| |LD|LS|RS|RD| 4 s
//   |-----------| |-----------|   [r]
// 3 |RS|RD|LD|LS| |RS|RD|LD|LS| 5 []
//   |-----------| |-----------|
// 2 |LS|LD|RD|RS| |LS|LD|RD|RS| 6
//   |-----------| |-----------|
// 1 |RD|RS|LS|LD| |RD|RS|LS|LD| 7
//   ------------- -------------
//     0  1  2  3   4  5  6  7
//         mSquares[][c]
//
// Quadrants
// 0: a-dx8-5
// 1: e-hx8-5
// 2: a-dx4-1
// 3: e-hx4-1
//
// Routes
// Left Dimond:		Left Square:	Right Dimond:	Right Square:	
// -------------	-------------	-------------	-------------
// |LD|  |  |  |	|  |LS|  |  |	|  |  |  |RD|	|  |  |RS|  |
// -------------	-------------	-------------	-------------
// |  |  |LD|  |	|  |  |  |LS|	|  |RD|  |  |	|RS|  |  |  |
// -------------	-------------	-------------	-------------
// |  |LD|  |  |	|LS|  |  |  |	|  |  |RD|  |	|  |  |  |RS|
// -------------	-------------	-------------	-------------
// |  |  |  |LD|	|  |  |LS|  |	|RD|  |  |  |	|  |RS|  |  |
// -------------	-------------	-------------	-------------
class Board
{
public:
	Board();
	
	// Print the chessboard to stdout
	void print();
	// Resets all the squares of the board to there default state
	void reset();
	// Solves Knight's Tour problem
	void solve(BoardPosition);
	// Visits the square at the defined position and stores the turn it is visited at.
	bool visitSquare(const BoardPosition& position, const int& turn);

private:
	// Assigns the correct route and quadrant for each square
	void initializeBoard();
	// Assigns the moves of the knight
	void initializeKnightMoves();

	// Looks for a visitable square from the specified position and assigns it in neighborPosition
	//
	// Parameters:
	// -> position:				The current position of the knight on the board.
	// -> inQuadrant:			Defines if we want to find a reachable position within the same quadrant.
	// -> inRoute:				Defines if we want to find a reachable position within the same route.
	// -> moveOffset:			The index of mKnightMoves position we should start looking at.
	// <- neighborsPosition:	The position of the first visitable square on the board.
	//
	// Return value:
	// On succes: the index of mKnightMoves[]
	// On fail:   -1
	int Board::getVisitableSquare(const BoardPosition& position, const bool& inQuadrant, const bool& inRoute, const int& moveOffset, BoardPosition* neighborPosition);
	// Looks and assigns the visitable positions within the quadrant, and the first outside the quadrant
	//
	// Parameter:
	//  -> inRoute:		Defines if we want to find a reachable position within the same route.
	// <-> qSP:			The position of the quadrant the knight started in
	//	->				The current position of the knight on the board.
	//  <-				The visitable position the knight will start in the next quadrant
	//  <- dN1:			The first position the kight will visit from the current kight's position within the same quadrant and route.
	//  <- dN2:			The second position the kight will visit from the current kight's position within the same quadrant and route.
	void Board::getNeighbors(const bool& inRoute, BoardPosition* qSP, BoardPosition* dN1, BoardPosition* dN2);
	// Return:
	// The square in mSquares[][] at the specified position
	Square* getSquare(const BoardPosition& position);
	// Checks if the specified position is within the 8x8 bounds 
	//
	// Return:
	// On succes: true
	bool positionInBounds(const BoardPosition& position);

	// A dashed line used in print()
	char* mDashedLine = nullptr;
	// The header and footer of a chessboard
	char* mHeader = nullptr;
	// The moves a knight can take viewed from the origin
	BoardPosition mKnightMoves[8];
	// The chess board
	Square mSquares[8][8];
};
#endif // KNIGHTSTOUR_BASE_HEADERFILES_BOARD_H