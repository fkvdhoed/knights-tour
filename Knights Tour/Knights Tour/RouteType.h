#ifndef KNIGHTSTOUR_BASE_HEADERFILES_ROUTETYPE_H
#define KNIGHTSTOUR_BASE_HEADERFILES_ROUTETYPE_H

// Defines what route type a square on the chessboard is in
//
// Only used in combination with Board.h and Square.h for solving the Kight's Tour problem
enum class RouteType
{
	UNDIFINED,
	LEFT_DIMOND,
	LEFT_SQUARE,
	RIGHT_DIMOND,
	RIGHT_SQUARE
};

#endif // KNIGHTSTOUR_BASE_HEADERFILES_ROUTETYPE_H