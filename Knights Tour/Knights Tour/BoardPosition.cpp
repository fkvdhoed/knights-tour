#include "BoardPosition.h"

#pragma region Constructors
BoardPosition::BoardPosition() : mColumn(0), mRow(0) {}
BoardPosition::BoardPosition(const int& column, const int& row) : mColumn(column), mRow(row) {}
#pragma endregion

#pragma region Default Getters
int BoardPosition::getColumn() const { return mColumn; }
int BoardPosition::getRow() const { return mRow; }
#pragma endregion

#pragma region Default Setters
void BoardPosition::setColumn(int value) { mColumn = value; }
void BoardPosition::setRow(int value) { mRow = value; }
#pragma endregion

#pragma region Operators
bool BoardPosition::operator == (const BoardPosition & rhs) const
{
	return mColumn == rhs.mColumn && mRow == rhs.mRow;
}

BoardPosition BoardPosition::operator + (const BoardPosition & rhs) const
{
	return BoardPosition(mColumn + rhs.mColumn, mRow + rhs.mRow);
}

BoardPosition BoardPosition::operator - (const BoardPosition & rhs) const
{
	return BoardPosition(mColumn - rhs.mColumn, mRow - rhs.mRow);
}

BoardPosition& BoardPosition::operator += (const BoardPosition & rhs)
{
	mColumn += rhs.mColumn;
	mRow += rhs.mRow;
	return *this;
}

BoardPosition& BoardPosition::operator -= (const BoardPosition & rhs)
{
	mColumn -= rhs.mColumn;
	mRow -= rhs.mRow;
	return *this;
}
#pragma endregion

void BoardPosition::set(const int& row, const int& column)
{
	mRow = row;
	mColumn = column;
}