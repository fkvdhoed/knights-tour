#include <cstdio>
#include <cstdlib>

#include "Board.h"

#pragma region Constructors
Board::Board()
{
	initializeBoard();
	initializeKnightMoves();
}
#pragma endregion

#pragma region Iniltializers
void Board::initializeBoard()
{
	int q = 0;
	for (int r = 0; r < 8; r += 4)
	{
		for (int c = 0; c < 8; c += 4)
		{
			mSquares[r][c].setQuadrant(q);
			mSquares[r][c].setRouteType(RouteType::LEFT_DIMOND);
			mSquares[r][c + 1].setQuadrant(q);
			mSquares[r][c + 1].setRouteType(RouteType::LEFT_SQUARE);
			mSquares[r][c + 2].setQuadrant(q);
			mSquares[r][c + 2].setRouteType(RouteType::RIGHT_SQUARE);
			mSquares[r][c + 3].setQuadrant(q);
			mSquares[r][c + 3].setRouteType(RouteType::RIGHT_DIMOND);

			mSquares[r + 1][c].setQuadrant(q);
			mSquares[r + 1][c].setRouteType(RouteType::RIGHT_SQUARE);
			mSquares[r + 1][c + 1].setQuadrant(q);
			mSquares[r + 1][c + 1].setRouteType(RouteType::RIGHT_DIMOND);
			mSquares[r + 1][c + 2].setQuadrant(q);
			mSquares[r + 1][c + 2].setRouteType(RouteType::LEFT_DIMOND);
			mSquares[r + 1][c + 3].setQuadrant(q);
			mSquares[r + 1][c + 3].setRouteType(RouteType::LEFT_SQUARE);

			mSquares[r + 2][c].setQuadrant(q);
			mSquares[r + 2][c].setRouteType(RouteType::LEFT_SQUARE);
			mSquares[r + 2][c + 1].setQuadrant(q);
			mSquares[r + 2][c + 1].setRouteType(RouteType::LEFT_DIMOND);
			mSquares[r + 2][c + 2].setQuadrant(q);
			mSquares[r + 2][c + 2].setRouteType(RouteType::RIGHT_DIMOND);
			mSquares[r + 2][c + 3].setQuadrant(q);
			mSquares[r + 2][c + 3].setRouteType(RouteType::RIGHT_SQUARE);

			mSquares[r + 3][c].setQuadrant(q);
			mSquares[r + 3][c].setRouteType(RouteType::RIGHT_DIMOND);
			mSquares[r + 3][c + 1].setQuadrant(q);
			mSquares[r + 3][c + 1].setRouteType(RouteType::RIGHT_SQUARE);
			mSquares[r + 3][c + 2].setQuadrant(q);
			mSquares[r + 3][c + 2].setRouteType(RouteType::LEFT_SQUARE);
			mSquares[r + 3][c + 3].setQuadrant(q);
			mSquares[r + 3][c + 3].setRouteType(RouteType::LEFT_DIMOND);

			++q;
		}
	}
}

void Board::initializeKnightMoves()
{
	mKnightMoves[0].set(-1, -2);
	mKnightMoves[1].set(-2, -1);
	mKnightMoves[2].set(-2, 1);
	mKnightMoves[3].set(-1, 2);
	mKnightMoves[4].set(1, 2);
	mKnightMoves[5].set(2, 1);
	mKnightMoves[6].set(2, -1);
	mKnightMoves[7].set(1, -2);
}
#pragma endregion

void Board::print()
{
	if (!mDashedLine)
	{
		const unsigned width = (4 * 8) + 3;
		const size_t msize = width + 1;
		mDashedLine = (char *)malloc(msize);
		mDashedLine[0] = ' ';
		mDashedLine[1] = ' ';
		for (unsigned i = 2; i < width; ++i) mDashedLine[i] = '-';
		mDashedLine[width] = '\0';
	}
	if (!mHeader)
	{
		mHeader = "   a   b   c   d   e   f   g   h";
	}

	printf("%s\n%s\n", mHeader, mDashedLine);
	for (int r = 0; r < 8; ++r)
	{
		printf("%d ", 8-r); // Display row number
		for (int c = 0; c < 8; ++c)
		{
			int turnVisited = mSquares[c][r].getTurnVisited();
			printf("| %d%s", turnVisited, turnVisited < 10 && -1 < turnVisited ? " " : ""); // Display square content
		}
		printf("| %d\n%s\n", 8 - r, mDashedLine);
	}
	printf("%s\n", mHeader);
}

void Board::reset()
{
	for (int r = 0; r < 8; ++r)
	{
		for (int c = 0; c < 8; ++c)
		{
			mSquares[r][c].reset();
		}
	}
}

// This algorithm divides a 8x8 chess board in 4 subquadrant and 4 routes
// It first visit all the reachable squares in the quadrant
// then it will visit an other quadrant with the same RouteType before it will start a new route
//
// LOOP:
// visit quadrantStartPos
// At the quadrantStartPos it looks for the visitable squares within the quadrant
// getNeighbors() will look for reachable squares in the quadrant and makes sure that step3 has a reachable square in the next quadrant
// it will aslso store the reachable position from step3 to the next quadrant, and possible route, in quadrantStartPos.
// NOTE: the origional quadrantStartPos can't be used after this
// After it visits the position of step1 it will look search a reachable position from that square and it will visit that
// visit the position of step3
// REPEAT
void Board::solve(BoardPosition quadrantStartPos)
{
	BoardPosition step1;
	BoardPosition step2;
	BoardPosition step3;
	int turn = -1;

	for(int r = 0; r < 4; ++r)  //for each route
	{
		for (int q = 0; q < 4; ++q)  //for each quadrant
		{
			visitSquare(quadrantStartPos, ++turn);
			getNeighbors(q == 3 ? false : true, &quadrantStartPos, &step1, &step3);
			visitSquare(step1, ++turn);
			getVisitableSquare(step1, true, true, 0, &step2);
			visitSquare(step2, ++turn);
			visitSquare(step3, ++turn);
		}
	}
}

bool Board::visitSquare(const BoardPosition& position, const int& turn)
{
	if (positionInBounds(position))
	{
		Square* square = getSquare(position);
		mSquares[position.getRow()][position.getColumn()];
		if (square->visit(turn))
		{
			return true;
		}
	}

	return false;
}

int Board::getVisitableSquare(const BoardPosition& position, const bool& inQuadrant, const bool& inRoute, const int& moveOffset,  BoardPosition* neighborPosition)
{
	if(positionInBounds(position))
	{
		Square* curSquare = getSquare(position);
		for (int mI = moveOffset; mI < 8; ++mI)
		{
			BoardPosition neighborPos = position + mKnightMoves[mI];
			if (positionInBounds(neighborPos))
			{
				Square* neighborSquare = getSquare(neighborPos);
				if (!neighborSquare->getVisited())
				{
					if ((curSquare->getRouteType() == neighborSquare->getRouteType() && inRoute)  // search inside route
						|| (curSquare->getRouteType() != neighborSquare->getRouteType() && !inRoute))  // search outside route
					{
						if (((curSquare->getQuadrant() == neighborSquare->getQuadrant() && inQuadrant)  // search inside quadrant
							|| (curSquare->getQuadrant() != neighborSquare->getQuadrant() && !inQuadrant)))  // search outside quadrant
						{
							*neighborPosition = neighborPos;
							return mI;
						}
					}
				}
			}
		}
	}

	return -1; // No non visitable square found
}

void Board::getNeighbors(const bool& inRoute, BoardPosition* qSP, BoardPosition* dN1, BoardPosition* dN2)
{
	int moveIndex = getVisitableSquare(*qSP, true, true, 0, dN1);  // NOTE: is not able to return -1 with the algorithm
	getVisitableSquare(*qSP, true, true, ++moveIndex, dN2);  // NOTE: is not able to return -1 with the algorithm
	
	// Looks for the next quadrant start position.
	// dN1 will be visited first so we only need to check if dN2 can visit a sqaure outside the quadrant
	if (getVisitableSquare(*dN2, false, inRoute, 0, qSP) == -1)  // If dN2 cann't visit a square ouside quadrant
	{															 // NOTE: getVisitableSquare() changes the last parameter so we cannot use the start position from the current quadrant anymore
		// Swap values
		BoardPosition temp = *dN1;
		*dN1 = *dN2;
		*dN2 = temp;

		getVisitableSquare(*dN2, false, inRoute, 0, qSP);  // NOTE: it will only return -1 in the last quadrant of the last route
	}
}

Square* Board::getSquare(const BoardPosition& position)
{
	return &mSquares[position.getRow()][position.getColumn()];
}

bool Board::positionInBounds(const BoardPosition& position)
{
	return -1 < position.getColumn() && position.getColumn() < 8 && -1 < position.getRow() && position.getRow() < 8;
}