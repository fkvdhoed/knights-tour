#include "Square.h"

#pragma region Constructors
Square::Square() : mQuadrant(-1), mRouteType(RouteType::UNDIFINED), mTurnVisited(-1), mVisited(false) {}
Square::Square(const int & quadrant, const RouteType & routeType) : mQuadrant(quadrant), mRouteType(routeType), mTurnVisited(-1), mVisited(false){}
#pragma endregion

#pragma region Default Getters
int Square::getQuadrant() { return mQuadrant; }
RouteType Square::getRouteType() { return mRouteType; }
int Square::getTurnVisited() { return mTurnVisited; }
bool Square::getVisited() { return mVisited; }
#pragma endregion
#pragma region Default Setters
void Square::setQuadrant(const int& value) { mQuadrant = value; }
void Square::setRouteType(const RouteType& value) { mRouteType = value; }
#pragma endregion

void Square::reset()
{
	mTurnVisited = -1;
	mVisited = false;
}

bool Square::visit(const int & turn)
{
	if (!mVisited)
	{
		mVisited = true;
		mTurnVisited = turn;
		return true;
	}
	else
	{
		return false;
	}
}

